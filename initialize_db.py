import sqlite3

conn = sqlite3.connect('menuS.db')
cursor = conn.cursor()

## Meal table creation 
## Meal id, Book id, Meal name, Cooking time, persons, page
q_meal = '''
    CREATE TABLE IF NOT EXISTS [meal] (
        meal_id INTEGER PRIMARY KEY AUTOINCREMENT,
        book_id INTEGER NULL,
        meal_name TEXT, 
        book_name TEXT,
        cooking_time FLOAT, 
        persons INTEGER, 
        page INTEGER,
        FOREIGN KEY (book_id) REFERENCES [book] (book_id)
    );
    '''



## Ingredient table creation 
q_ing = '''
    CREATE TABLE IF NOT EXISTS [ingredient] (
        ing_id INTEGER PRIMARY KEY AUTOINCREMENT, 
        name TEXT, 
        unit TEXT,
        type TEXT  
    );
'''


## Book table creation 
## Book id, book name, author
q_book = '''
    CREATE TABLE IF NOT EXISTS [book] (
        book_id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT, 
        first_name TEXT,
        last_name TEXT
    );
''' 


## Meal-Ingredient table 
## Meal id, Ingredient id, quantity, unit
q_mi = '''
    CREATE TABLE IF NOT EXISTS [meal_ing] (
        meal_id INTEGER,
        ing_id INTEGER,
        meal_name TEXT, 
        ing_name TEXT,  
        quantity FLOAT,
        unit TEXT,
        FOREIGN KEY (meal_id) REFERENCES [meal] (meal_id),
        FOREIGN KEY (ing_id) REFERENCES [ingredient] (ing_id)
    );
'''

query_list = [q_book, q_meal, q_ing, q_mi]

for q in query_list:
    cursor.execute(q)

conn.commit()

## Debugging
