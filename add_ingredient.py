from audioop import add
import pandas as pd
import numpy as np
from copy import deepcopy
import sqlite3 
import itertools
import datetime



conn = sqlite3.connect('menuS.db')

q_cing = '''
    CREATE TABLE IF NOT EXISTS [custom_ing] (
        cing_id INTEGER PRIMARY KEY AUTOINCREMENT,
        ing_name TEXT,
        quantity FLOAT,
        unit TEXT,
        time_add TEXT
    );
    '''

cursor = conn.cursor()
cursor.execute(q_cing)
conn.commit()

def add_ing(ing_name, quantity=1, unit='unit'):
    # Takes an ingredient name 
    # and add it to the custom_ing table 
    q_ins = '''
        INSERT INTO custom_ing (ing_name, quantity, unit, time_add)
        VALUES (?, ?, ?, ?);
    '''

    n = len(ing_name)

    if not isinstance(quantity, list):
        quantity = list(itertools.repeat(quantity, n))
    if not isinstance(unit, list):
        unit = list(itertools.repeat(unit, n))

    date_now = list(itertools.repeat(str(datetime.date.today()), n))
    
    data = [(x, y, z, d) for x, y, z, d in zip(ing_name, quantity, unit, date_now)]

    cursor.executemany(q_ins, data)
    conn.commit()

def purge_cing():
    q_purge = '''
        DROP TABLE IF EXISTS [custom_ing];
    '''
    cursor.execute(q_purge)
    # cursor.execute(q_cing)
    conn.commit()

## Run 
ing_names = ['chinese_noodles', 'toilet_paper', 'pesto', 
    'spaghetti', 'milk', 'coffee', 'wet_cat_food']

add_ing(ing_names)

q_getIngs = '''
    SELECT ing_name, quantity, unit, 'auto' as type 
    FROM choice_today
    UNION 
    SELECT ing_name, quantity, unit, 'custom' as type
    FROM custom_ing
'''

allIng_dat = pd.read_sql(q_getIngs, conn).sort_values(['type', 'ing_name'])

allIng_dat.to_csv('ing_list.csv', index=False, sep=';')

purge_cing()


