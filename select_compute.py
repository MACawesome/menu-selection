import sqlite3 
import pandas as pd
import numpy as np
import datetime
import re 

log_date = str(datetime.datetime.today())
date_pattern = r'\d+-\d+-\d+'
log_date = re.search(date_pattern, log_date)[0]

conn = sqlite3.connect('menuS.db')
cursor = conn.cursor()

q_meals = '''
    SELECT meal_id, meal_name, persons, book_name, page
    FROM meal;
'''

meal_dat = pd.read_sql(q_meals, conn)
# meal_ids = [str(id) for id in meal_dat['meal_id'].values]

n_meals = 4
n_persons = 2

idx = np.random.choice(range(meal_dat.shape[0]), n_meals, replace=False)

select_dat = meal_dat.iloc[idx, :]

select_dat = select_dat.rename({'meal_id':'meal_choice'}, axis=1)
select_dat= select_dat.astype({'meal_choice': int})

print('Meal selection: {}'.format(select_dat['meal_name'].values))

## Delete rows in current log date and append new ones
q_exists = '''
    SELECT 1 
    FROM sqlite_master 
    WHERE type='table' AND name='tmp_selection';
'''

exist_cond = len(cursor.execute(q_exists).fetchall())

q_delDate = '''
        DELETE FROM tmp_selection
        WHERE log_date = '{}';
'''.format(log_date)

if exist_cond > 0:
    cursor.execute(q_delDate)
    conn.commit()

select_dat['log_date'] = log_date

select_dat.loc[:, ['meal_choice', 'meal_name', 'persons']].\
    to_sql('tmp_selection', conn, if_exists='append', index=False)

mealing_dat = pd.read_sql('SELECT * FROM meal_ing', conn)

choice_dat = pd.merge(select_dat, mealing_dat, how='left', 
        left_on='meal_choice', 
        right_on='meal_id')


# # How nice is the solution?
# from scipy.stats import entropy
# p_x = np.ones(meal_dat.shape[0])/meal_dat.shape[0]

# tb_arr = np.unique(meal_names, return_counts=True)
# cnt_dat = pd.DataFrame({'meal_name': tb_arr[0], 'cnt': tb_arr[1]})

# mc_dat = pd.merge(meal_dat, cnt_dat, how='left', on='meal_name')

# mc_dat['cnt'][~mc_dat['cnt'].notna()] = 0

# q_x = mc_dat['cnt'].values

# # entropy(p_x, q_x)


## Perform ingredient summations based on meal sample 
choice_dat['quantity'] = choice_dat['quantity']/choice_dat['persons'] * n_persons

choice_dat = choice_dat.groupby(['ing_name', 'unit'])['quantity'].sum().reset_index()

## Append date of computation 
log_date = str(datetime.datetime.today())
date_pattern = r'\d+-\d+-\d+'
log_date = re.search(date_pattern, log_date)[0]

choice_dat['log_date'] = log_date 

## Delete rows in current log date and append new ones
q_exists = '''
    SELECT 1 
    FROM sqlite_master 
    WHERE type='table' AND name='choice';
'''

exist_cond = len(cursor.execute(q_exists).fetchall())

q_delDate = '''
        DELETE FROM choice
        WHERE log_date = '{}';
'''.format(log_date)

if exist_cond > 0:
    cursor.execute(q_delDate)
    conn.commit()

choice_dat.to_sql('choice', conn, if_exists='append', index=False)

## Output today's choice 
q_choiceToday = '''
    SELECT * 
    FROM choice_today;
'''

pd.read_sql(q_choiceToday, conn).to_csv('./output/current_choices_{}.csv'.format(log_date), index=False, sep=';')
select_dat.to_csv('./output/selection-{}.csv'.format(log_date), index=False, sep=';')

## Output (historic)
choice_dat.to_csv('./output/choices-{}.csv'.format(log_date), index=False, sep=';')











