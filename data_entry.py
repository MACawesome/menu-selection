## Dictionary containing set-up 
## with meal, 
# time to prepare meal
# book from which the meal comes from 
# page in book
# number of persons
# list of ingredients, list of quantities per ingredient
# list of unit per ingredient

## parse values in excel sheet and insert into dictionary 

import pandas as pd
import numpy as np
from copy import deepcopy
import sqlite3 

conn = sqlite3.connect('menuS.db')
# conn.cursor()

meal_data = pd.read_excel('meal_input.ods')

meal_dict = {'meal': '', 
            'book': '', 
            'page': None,
            'prep_time': None, 
            'n_persons': None,
            'ingredients': [],
            'quantities': [],
            'units': []}

def parse_mealTab(mealTab):
    ## Take in meal DataFrame.
    ## Returns a list of dictionary with the info. 
    mealTab.columns = list(map(lambda x: x.lower().strip(), mealTab.columns))
    listMeals = []

    for i in range(mealTab.shape[0]):
        out_dict = deepcopy(meal_dict)

        row = mealTab.iloc[i, :]
        out_dict['meal'] = row['meal']
        out_dict['book'] = row['book']
        out_dict['page'] = row['page']
        out_dict['prep_time'] = row['prep. time']
        out_dict['n_persons'] = row['n. persons']
        out_dict['ingredients'] = list(map(lambda x: x.strip(), row['ingredients'].split(';')))
        out_dict['quantities'] = list(map(lambda x: float(x.strip()), row['quantity'].split(';')))
        out_dict['units'] = list(map(lambda x: x.strip(), row['units'].split(';')))

        listMeals.append(out_dict)

    return listMeals

list_meals = parse_mealTab(meal_data)

## Delete meal info already in database 
existing_meals = [d['meal'] for d in list_meals]

qDelexist = '''
    DELETE FROM {0}
    WHERE {1} IN ('{2}');
'''

cursor = conn.cursor()
cursor.execute(qDelexist.format('meal', 'meal_name', '\', \''.join(existing_meals)))
cursor.execute(qDelexist.format('meal_ing', 'meal_name', '\', \''.join(existing_meals)))
conn.commit()

## Insertion queries
q_meal = '''
    INSERT INTO meal (meal_name, book_name, cooking_time, persons, page)
    VALUES (?, ?, ?, ?, ?);
'''

q_ing = '''
    INSERT INTO ingredient (name, unit)
    VALUES (?, ?);
'''

q_book = '''
    INSERT INTO book (name)
    VALUES (?);
'''

q_mi = '''
    INSERT INTO meal_ing (meal_name, ing_name, quantity, unit)
    VALUES (?, ?, ?, ?);
'''

def post_mealDict(listMeals, conn):
    ## Take in list of meal dictionaries
    ## Load them into the database. 
    rows_meal = []
    rows_book = []
    rows_ings = []
    rows_mi = []

    all_ings = []
    all_units = []
    all_meals = []
    all_quants = []

    ## Load the rows of data to input to the 
    ## meal, book, meal-ing and ing tables. 
    for i in range(len(listMeals)):
        row_meal = (listMeals[i]['meal'], 
                    listMeals[i]['book'], 
                    str(listMeals[i]['prep_time']), 
                    str(listMeals[i]['n_persons']),
                    str(listMeals[i]['page']))
        rows_meal.append(row_meal)

        row_book = (listMeals[i]['book'])
        rows_book.append(row_book)

        all_ings += listMeals[i]['ingredients']
        all_units += listMeals[i]['units']
        all_quants += listMeals[i]['quantities']

        meals = [listMeals[i]['meal'] for j in range(len(listMeals[i]['ingredients']))]
        all_meals += meals

    rows_book = [tuple([o]) for o in rows_book]

    ing_vec, unit_vec = ([], [])
    for ing, unit in zip(all_ings, all_units):
        if not ing in ing_vec:
            ing_vec.append(ing)
            unit_vec.append(unit)
        else:
            pass
    
    for i in range(len(all_meals)):
        rows_mi.append((all_meals[i], all_ings[i], str(all_quants[i]), all_units[i]))

    for i in range(len(ing_vec)):
        rows_ings.append((ing_vec[i], unit_vec[i]))

    ## Insert the data in the relevant tables 
    ## using the hard-coded queries. 
    cursor = conn.cursor()
    cursor.executemany(q_meal, rows_meal)
    cursor.executemany(q_ing, rows_ings)
    cursor.executemany(q_book, rows_book)
    cursor.executemany(q_mi, rows_mi)

    conn.commit()

    return None

post_mealDict(list_meals, conn)

## Remove duplicates based on the GROUP BY values 
cursor = conn.cursor()

q_delDup = '''
    DELETE FROM {0} 
    WHERE rowid NOT IN (
        SELECT MAX(rowid)
        FROM {0}
        GROUP BY {1}
    );
'''

cursor.execute(q_delDup.format('meal', 'meal_name'))
cursor.execute(q_delDup.format('book', 'name'))
cursor.execute(q_delDup.format('ingredient', 'name'))
cursor.execute(q_delDup.format('meal_ing', ', '.join(['meal_name', 'ing_name'])))

conn.commit()

## Set meal_id and ing_id values on the meal_ing table 
q_updateMI = '''
    UPDATE meal_ing
    SET meal_id = (
        SELECT meal.meal_id
        FROM meal
        WHERE meal_ing.meal_name = meal.meal_name
    )
    , ing_id = (
        SELECT ingredient.ing_id
        FROM ingredient
        WHERE meal_ing.ing_name = ingredient.name
    );
'''

cursor.execute(q_updateMI)
conn.commit()


## Set book_id on the meal table 
q_updateMeal = '''
    UPDATE meal
    SET book_id = (
        SELECT book.book_id
        FROM book 
        LEFT JOIN meal ON meal.book_name = book.name
    );
'''

cursor.execute(q_updateMeal)
conn.commit()








    






    

        

